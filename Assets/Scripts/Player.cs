using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player 
{
    private string _username;
    private int _score;
    private string _password;
    public string Username
    {
        get => _username;
        set => _username = value;
    }

    public int Score
    {
        get => _score;
        set => _score = value;
    }

    public string Password
    {
        get => _password;
        set => _password=value;
    }

    public Player(string username, int score, string password)
    {
        _username = username;
        _score = score;
        _password = password;
    }
}
