using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRun : MonoBehaviour
{
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (GameManager.Instance.money<6)
        {
            anim.speed = 1;
        }
        else
        {
            anim.speed = GameManager.Instance.money / 6;
        }
        // anim["Run"].speed += GameManager.Instance.money;
    }
}
