using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

//Boton de la pantalla de inicio que lleva al menu. Por lo tanto gestiona que se cumplan los requisitos del nombre. No vac�o y mayor que dos.
public class GoToMenu : ButtonChangeScene
{
    private GameObject _popUp;
    // Start is called before the first frame update
    // Update is called once per frame

    protected override void Start()
    {
        base.Start();
        //Oculta el Pop-Up
        _popUp = GameObject.Find("Pop-Up");
        _popUp.SetActive(false);
    }
    protected override void TaskOnClick()
    {
        //Busca el nombre del jugador dentro del GameObject persistente para confirmar que no esta vaci� o es mayor que dos.
        Debug.Log(GameObject.Find("InsertName").name);
        string username = GameObject.Find("NameHolder").GetComponent<TMP_Text>().text;
        var password = GameObject.Find("PasswordHolder").GetComponent<TMP_Text>().text;
        if (username.Length > 3&&password.Length>=8)
        {
            var player = new Player(username, 0, password);
            ApiService.Instance.PostData(player);
            GameManager.Instance.SetCurrentPlayer(player);
            GameManager.Instance.ActivateStartGame();
            UIManager.Instance.ClickButtonChangeScene("ClickerGame");
        }
        else
        {
            //Muestra el pop-up
            _popUp.SetActive(true);
            //Funcion que se encarga de hacer esperar 1.5 segundos antes de desaparecer el pop.up. 
            StartCoroutine(CloseAfterTime(1.5f));
        }              
    }

    public IEnumerator CloseAfterTime(float t)
    {
        yield return new WaitForSeconds(t);
        _popUp.SetActive(false);
    }
}
