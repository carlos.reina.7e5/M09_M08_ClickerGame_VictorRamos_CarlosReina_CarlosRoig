using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class UIManager : MonoBehaviour
{
    private static UIManager _instance;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("UI Manager is NULL");
            }
            return _instance;
        }
    }

    private void Awake()
    {
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ClickButtonChangeScene(string scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void CallGetApi()
    {
        ApiService.Instance.GetData();
    }


    public void ShowPlayerRanking(List<Player> players)
    {
        OrderByScore(ref players);
        GameObject[] playerHolder =
        {
            GameObject.Find("PlayerHolder1"),
            GameObject.Find("PlayerHolder2"),
            GameObject.Find("PlayerHolder3"),
            GameObject.Find("PlayerHolder4"),
            GameObject.Find("PlayerHolder5"),
            GameObject.Find("PlayerHolder6"),
        };
        for(var i = 0; i < playerHolder.Length; i++)
        {
            playerHolder[i].transform.Find("Username").GetComponent<TMPro.TMP_Text>().text = "Username : " + players[i].Username;
            playerHolder[i].transform.Find("Score").GetComponent<TMPro.TMP_Text>().text = "Score : " + players[i].Score;
        }
    }

    private void OrderByScore(ref List<Player> players)
    {
        for (var i = 0; i < players.Count; i++)
        {
            for (var y = i + 1; y < players.Count; y++)
            {
                if (players[i].Score < players[y].Score)
                {
                    (players[i], players[y]) = (players[y], players[i]);
                }
            }
        }
    }
}
