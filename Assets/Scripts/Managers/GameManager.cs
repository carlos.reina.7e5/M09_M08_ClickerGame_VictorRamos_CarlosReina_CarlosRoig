using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public TextMeshProUGUI puntuacioText;
    public static GameManager Instance { get; private set; }
    public int butonnormal = 1;
    public int booster = 5;
    public int money;
    private Player _currentPlayer;
    private bool _startGame;


    public float Money
    {
        get { return money; }
    }
    

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Debug.Log("Hay mas de un Game Manager en la escena");
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "ClickerGame")
        {
            if (_startGame)
            {
                puntuacioText = GameObject.Find("Score").GetComponent<TextMeshProUGUI>();
                _startGame = false;
            }
        }
    }
    public void ActivateStartGame()
    {
        _startGame = true;
    }

    public void SumarPuntos(int num)
    {
        money += num;
        puntuacioText.text = money.ToString();
    }
    public void QuitarPuntos()
    {
        money /= 2;
        puntuacioText.text = money.ToString();
    }

    public void SetCurrentPlayer(Player player)
    {
        _currentPlayer = player;
    }

    public Player GetCurrentPlayerPoints()
    {
        _currentPlayer.Score = money;
        return _currentPlayer;
    }
}
