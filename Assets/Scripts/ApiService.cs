using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using System.Text;
using System;

public class ApiService : MonoBehaviour
{
    private const string uri = "http://localhost:3000/players";
    // Start is called before the first frame update
    private static ApiService _instance;
    public static ApiService Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("Api Service is NULL");
            }
            return _instance;
        }
    }
    private void Awake()
    {
        if (_instance != null)
            Destroy(gameObject);
        else
        {
            DontDestroyOnLoad(gameObject);
            _instance = this;
        }
    }
    private void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void GetData() => StartCoroutine(GetData_Coroutine());

    IEnumerator GetData_Coroutine()
    {
        using UnityWebRequest request = UnityWebRequest.Get(uri + "/ranking");
        yield return request.SendWebRequest();

        string[] pages = uri.Split('/');
        int page = pages.Length - 1;

        if (request.result == UnityWebRequest.Result.ConnectionError)
        {
            Debug.Log(pages[page] + ": Error: " + request.error);
        }
        else
        {
            Debug.Log(pages[page] + ":\nReceived: " + request.downloadHandler.text);
            var courses = request.downloadHandler.text;
            string[] jsons = new string[1];
            bool start = false;
            for (var i = 0; i < courses.Length; i++)
            {
                if (courses[i].ToString().Contains("]"))
                    start = false;
                if (start)
                {
                    jsons[^1] += courses[i];
                    if (courses[i].ToString().Contains("}"))
                    {
                        Array.Resize(ref jsons, jsons.Length + 1);
                        i += 1;
                    }
                }
                if (courses[i].ToString().Contains("["))
                    start = true;
            }
            List<Player> players = new();
            foreach (var json in jsons)
            {
                Debug.Log(json);
                if (!string.IsNullOrEmpty(json))
                {
                    players.Add(JsonConvert.DeserializeObject<Player>(json));
                }
            }
            UIManager.Instance.ShowPlayerRanking(players);
        }
    }

    public void PostData(Player player) => StartCoroutine(PostData_Coroutine(player));

    IEnumerator PostData_Coroutine(Player player)
    {
        var json = "{\"username\": \"" + player.Username + "\", \"score\": " + player.Score + ", \"password\" : \"" + player.Password + "\"}";
        Debug.Log(json);
        using UnityWebRequest request = UnityWebRequest.Post(uri, json);
        byte[] codifyJson = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = new UploadHandlerRaw(codifyJson);
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.ConnectionError)
            Debug.LogError(request.error);
        else
            Debug.Log(request.downloadHandler.text);
    }

    public void PutData(Player player) => StartCoroutine(PutData_Coroutine(player));

    IEnumerator PutData_Coroutine(Player player)
    {


        var json = "{\"username\": \"" + player.Username + "\", \"score\": " + player.Score + ", \"password\" : \"" + player.Password + "\"}";
        using UnityWebRequest request = UnityWebRequest.Put(uri + "/Score", json);
        byte[] codifyJson = Encoding.UTF8.GetBytes(json);
        request.uploadHandler = new UploadHandlerRaw(codifyJson);
        request.SetRequestHeader("Content-Type", "application/json");
        yield return request.SendWebRequest();
        if (request.result == UnityWebRequest.Result.ConnectionError)
            Debug.LogError(request.error);
        else
            Debug.Log(request.downloadHandler.text);
    }
}
