using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonClicker : MonoBehaviour
{
    public int tiempoBoost =5;
    public GameObject[] botones;
    public AudioClip[] audios;

    public void Clicker()
    {
        GameManager.Instance.SumarPuntos(GameManager.Instance.butonnormal);
    }
    public void Booster()
    {
        StartCoroutine(BotonDesactivado(12,0));
        StartCoroutine(AumentoPunt(tiempoBoost));
        Clicker();
    }

    IEnumerator AumentoPunt(float tiempoBoost)
    {
        var puntosnormales = GameManager.Instance.butonnormal;
        GameManager.Instance.butonnormal = GameManager.Instance.booster;
        AudioManager.Instance.EjecutarSonido(audios[0]);
        yield return new WaitForSeconds(tiempoBoost);
        GameManager.Instance.butonnormal = puntosnormales;
    }


    public void Bancarrota()
    {
        StartCoroutine(BotonDesactivado(10, 1));
        AudioManager.Instance.EjecutarSonido(audios[1]);
        GameManager.Instance.QuitarPuntos();
    }
    IEnumerator BotonDesactivado(float tiempoOculto, int numboton)
    {
        botones[numboton].SetActive(false);

        yield return new WaitForSeconds(tiempoOculto);
        botones[numboton].SetActive(true);
    }

    public void Cerrar()
    {

        ApiService.Instance.PutData(GameManager.Instance.GetCurrentPlayerPoints());
        SceneManager.LoadScene(2);
    }
}
