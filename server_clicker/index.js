// Import necessary packages
const express = require('express');
const bodyParser = require('body-parser');
const { error } = require('console');

// create and configure the express app
const PORT = process.env.PORT || 3000;
const app = express();
app.use(express.json());

// Database Connection Info
const MongoClient = require('mongodb').MongoClient;

// socket channel
const server = require('http').createServer();
const io = require('socket.io')(server);

// the URL we copied from earlier. Replace Username and Password with what you created in the initial steps
const url = 'mongodb+srv://user:pass@cluster0.zj6x191.mongodb.net/?retryWrites=true&w=majority';
let db;

// The index route
app.get('/', function(req, res) {
   res.send('Clicker Game Ranking');
});

// Connect to the database with [url]
(async () => {
   let client = await MongoClient.connect(
       url,
       { useNewUrlParser: true }
   );

   db = client.db('Players');

   app.listen(PORT, async function() {
       console.log(`Listening on Port ${PORT}`);
       if (db) {
           console.log('Database is Connected!');
       }
   });
})();

// connection to socket
io.on('connection',(socket) => {
    console.log('a user connected');

    // manage Score update
    socket.on('Score-update', (data) => {
        // update Score
        db.collection('players').updateOne({username: data.username} , {$set: { Score: data.score}})
        .then(result => {
            console.log('Score updated: ', data)

            // emit update to all connected clients
            io.emit('Score-updated', data)
        })
        .catch(error => {
            console.error(error);
        });
    });
});

// Route to create new player
app.post('/players', async function(req, res) {
   // get information of player from POST body data
    let {username, score, password } = req.body;

   // check if the Username already exists
   const alreadyExisting = await db
       .collection('players')
       .findOne({ username: username });


    if (isNaN(score)){
        res.send({ status: false, msg: 'La puntuación tiene que ser un número' + typeof(score) }); 
    }
    else{
        if (username == "" || score < 0 || password == ""){
            res.send({ status: false, msg: 'values cannot be null and the Score must be higher than 0' }); 
        }
        else{
            if (password.length >= 8){
                if (alreadyExisting){
                    res.send({ status: false, msg: 'player Username already exists' });
                }
                else{
                    await db.collection('players').insertOne({ username, score, password });
                    console.log(`Created Player ${username}`);
                    res.send({ status: true, msg: 'player created' });
                }
            }
            else{
                res.send({ status: false, msg: 'the Password must have a minimum of 8 characters' });
            }
        }
    }
    
});


// update player Score
app.put('/players/Score', async function(req, res) {
    let { username, score } = req.body;
    // check if the Username already exists
    const alreadyExisting = await db
        .collection('players')
        .findOne({ username: username });
    if (alreadyExisting) {
        // Update player object with the Username
        if (isNaN(score)){
            res.send({ status: false, msg: 'La puntuación tiene que ser un número' }); 
        }
        else {
            await db
                .collection('players')
                .updateOne({ username }, { $set: { username, score } });
            console.log(`Player ${username} Score updated to ${score}`);
            res.send({ status: true, msg: 'player Score updated' });
        }
    } else {
        res.send({ status: false, msg: 'player Username not found' });
    }
 });

 // update player Score
app.put('/players/Username', async function(req, res) {
    let { oldUsername, newUsername } = req.body;
    // check if the Username already exists
    const alreadyExisting = await db
        .collection('players')
        .findOne({ username: oldUsername });
    if (alreadyExisting) {
        // Update player object with the Username
        if (newUsername != ""){
            await db
                .collection('players')
                .updateOne({ username: oldUsername }, { $set: { username: newUsername } });
            console.log(`Player ${oldUsername} Username updated to ${newUsername}`);
            res.send({ status: true, msg: 'player Username updated' });
        }
        else {
            res.send({ status: false, msg: 'values cannot be null'});
        }
    } else {
        res.send({ status: false, msg: 'player Username not found' });
    }
 });


// delete player
app.delete('/players', async function(req, res) {
    let { username, score } = req.body;
    // check if the Username already exists
    const alreadyExisting = await db
        .collection('players')
        .findOne({ username: username });
 
    if (alreadyExisting) {
        await db.collection("players").deleteOne({ username });
        console.log("Player ${Username} deleted");
        res.send({ status: true, msg: "player deleted" });
    } else {
        res.send({ status: false, msg: "Username not found" });
    }
 });
 
// Access the leaderboard
app.get('/players/Individual', async (req, res) => {
    let { username } = req.query;

    const alreadyExisting = await db
        .collection('players')
        .findOne({ "username": username });

    if (alreadyExisting) {
        let collection = await db.collection("players");
        let results = await collection.findOne({"username" : username});
        res.json({username: results.username, score: results.score});
    }
    else
        res.send({status: false, msg: "Not found: " + username})    
});

app.get('/players/ranking', async (req, res) => {

    let collection = await db.collection("players");
    let results = await collection.find({}).toArray()/*.sort(-1).limit(6)*/;
    res.send(results);
});